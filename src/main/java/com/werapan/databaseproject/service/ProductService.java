/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.werapan.databaseproject.service;

import com.werapan.databaseproject.dao.ProductDao;
import com.werapan.databaseproject.model.Product;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ProductService {
    
    public List<Product> getProducts(){
        ProductDao customerDao = new ProductDao();
        return customerDao.getAll(" product_id asc");
    }

    public Product addNew(Product editedProduct) {
        ProductDao customerDao = new ProductDao();
        return customerDao.save(editedProduct);
    }

    public Product update(Product editedProduct) {
        ProductDao customerDao = new ProductDao();
        return customerDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        ProductDao customerDao = new ProductDao();
        return customerDao.delete(editedProduct);
    }
}
